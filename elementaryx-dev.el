;;; elementaryx-full.el --- Further packaging of emacs elementaryx starter kit for HPC development and literate programming  -*- lexical-binding: t; -*-

;; Copyright (C) 2023, 2024  Emmanuel Agullo

;; Author: Emmanuel Agullo <eagullo@tek11>
;; Keywords: emacs emacs-elementaryx emacs-starter-kit ide hpc

(use-package elementaryx-dev-minimal)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Tree-sitter
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package emacs
  :config

  ;; Treesitter config: remap classical mode to ts-mode
  (add-to-list 'major-mode-remap-alist '(bash-mode . bash-ts-mode))
  ;; (add-to-list 'major-mode-remap-alist '(bibtex-mode . bibtex-ts-mode)) ;; TODO: investigate bug
  (add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
  (add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
  (add-to-list 'major-mode-remap-alist
               '(c-or-c++-mode . c-or-c++-ts-mode))
  (add-to-list 'major-mode-remap-alist '(css-mode . css-ts-mode))
  ;; (add-to-list 'major-mode-remap-alist '(html-mode . html-ts-mode))
  (add-to-list 'major-mode-remap-alist '(java-mode . java-ts-mode))
  (add-to-list 'major-mode-remap-alist '(javascript-mode . js-ts-mode))
  (add-to-list 'major-mode-remap-alist '(js-mode . js-ts-mode)) ;;
  (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))

  ;; Treesitter config: directly map to ts-mode
  (add-to-list 'auto-mode-alist '("CMakeLists\\.txt\\'" . cmake-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.cmake\\'" . cmake-ts-mode))
  (add-to-list 'auto-mode-alist '("\\Dockerfile\\'" . dockerfile-ts-mode))
  ;; (add-to-list 'auto-mode-alist '("\\.hs\\'" . haskell-ts-mode)) ;; TODO: investigate bug
  ;; (add-to-list 'auto-mode-alist '("\\.lhs\\'" . haskell-ts-mode)) ;; TODO: investigate bug
  ;; (add-to-list 'auto-mode-alist '("\\.jl\\'" . julia-ts-mode)) ;; TODO: investigate bug
  (add-to-list 'auto-mode-alist '("\\.json\\'" . json-ts-mode))
  ;; (add-to-list 'auto-mode-alist '("\\.lua\\'" . lua-ts-mode)) ;; TODO: investigate bug
  ;; (add-to-list 'auto-mode-alist '("\\.php\\'" . php-ts-mode)) ;; TODO: investigate bug
  (add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-ts-mode))
  (add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-ts-mode))
  ;; (add-to-list 'auto-mode-alist '("\\.ml\\'" . ml-ts-mode)) ;; TODO: not sure how to set it up with tree-sitter-ocaml
  ;; (add-to-list 'auto-mode-alist '("\\.\\(e?ya?\\|ra\\)ml\\'" . yaml-ts-mode)) ;; https://issues.guix.gnu.org/66836

  :hook
  ;; Auto parenthesis matching
  ((prog-mode . electric-pair-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                                          ;;;
;;;   Fall back to standard (non tree-sitter) mode when not available (or no solution found) ;;;
;;;                                                                                          ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://www.reddit.com/r/emacs/comments/1bqichi/did_anyone_manage_to_setup_markdowntsmode/
;; It seems that there is no tree sitter mode for mark down
;; (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-ts-mode)) ;; TODO: investigate bug
;; (add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-ts-mode)) ;; TODO: investigate bug
;; We fall back to https://jblevins.org/projects/markdown-mode/
(use-package markdown-mode
  :mode ("README\\.md\\'" . gfm-mode)
  :init (setq markdown-command "multimarkdown"))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Compiler output viewer
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; https://github.com/emacsmirror/rmsbolt
(use-package rmsbolt
  :bind (:map toggle-elementaryx-map
	      ("r" . rmsbolt-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Envrc (aka direnv)
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; We use https://github.com/purcell/envrc rather than
;; https://github.com/wbolster/emacs-direnv to set environment
;; variables buffer-locally.
(use-package envrc
  :bind (:map envrc-mode-map
	      ("C-c e" . envrc-command-map))
  :config (which-key-add-key-based-replacements "C-c e" "envrc")
  :init (envrc-global-mode))

(provide 'elementaryx-dev)
